//
//  Magic.swift
//  Ticker
//
//  Created by Vasyl Liutikov on 02.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation

struct Magic {
    // swiftlint:disable line_length
    fileprivate static let salt = "magic"
    fileprivate static let key: [UInt8] = [34, 53, 54, 30, 57, 7, 40, 31, 51, 9, 6, 84, 61, 3, 7, 4, 47, 35, 48, 87, 55, 38, 45, 4, 46, 42, 59, 11, 51, 55, 6, 24, 42, 4, 54, 94, 47, 35, 44, 82, 34, 37, 38]
    
    static func tokenKey() -> String {
        return reveal(key: key)
    }
    
    fileprivate static let secret: [UInt8] = [35, 53, 61, 0, 46, 58, 52, 82, 39, 39, 47, 9, 41, 45, 34, 92, 47, 35, 32, 27, 34, 37, 12, 19, 45, 23, 36, 84, 48, 9, 32, 80, 41, 61, 45, 7, 46, 32, 44, 27, 35, 27, 15, 1, 57, 42, 55, 14, 39, 55, 1, 11, 62, 3, 0, 89, 44, 29, 2, 83, 32, 37, 54, 92, 58, 0, 59, 13, 39, 39, 63, 11, 62, 19, 57, 0, 46, 35, 36, 80, 35, 12, 61, 5, 45, 26]
    
    static func tokenSecret() -> String {
        return reveal(key: secret)
    }
    // swiftlint:enable line_length
    fileprivate static func bytesByObfuscatingString(string: String) -> [UInt8] {
        let text = [UInt8](string.utf8)
        let cipher = [UInt8](Magic.salt.utf8)
        let length = cipher.count
        
        var encrypted = [UInt8]()
        
        for (index, element) in text.enumerated() {
            encrypted.append(element ^ cipher[index % length])
        }
        
        #if DEBUG
            print("Salt used: \(Magic.salt)\n")
            print("Swift Code:\n************")
            print("// Original \"\(string)\"")
            print("let key: [UInt8] = \(encrypted)\n")
        #endif
        
        return encrypted
    }
    
    /**
     This method reveals the original string from the obfuscated
     byte array passed in. The salt must be the same as the one
     used to encrypt it in the first place.
     
     - parameter key: the byte array to reveal
     
     - returns: the original string
     */
    fileprivate static func reveal(key: [UInt8]) -> String {
        let cipher = [UInt8](Magic.salt.utf8)
        let length = cipher.count
        
        var decrypted = [UInt8]()
        
        for (index, element) in key.enumerated() {
            decrypted.append(element ^ cipher[index % length])
        }
        
        return String(bytes: decrypted, encoding: String.Encoding.utf8)!
    }
}
