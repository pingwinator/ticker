//
//  HistoryViewController.swift
//  Ticker
//
//  Created by Vasyl Liutikov on 02.02.18.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import UIKit
import TickerNetwork

class HistoryViewController: UITableViewController {
    
    let priceFormatter = NumberFormatter.priceUSD
    var items: [TickerHistoryItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    static let cellId = "HistoryCellId"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 44.0
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: HistoryViewController.cellId, for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        cell.textLabel?.text = priceFormatter.string(for: item.average) ?? ""
        cell.detailTextLabel?.text = item.displayTimestamp
    }
    
}
