//
//  ViewController.swift
//  Ticker
//
//  Created by Vasyl Liutikov on 01.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import UIKit
import TickerNetwork

class ViewController: UIViewController {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var lastLabel: UILabel!
    @IBOutlet var askLabel: UILabel!
    @IBOutlet var bidLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!
    
    let priceFormatter = NumberFormatter.priceUSD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "BTC/USD"
        refresh()
    }

    @IBAction func refresh() {
        refreshButton.isEnabled = false
        API.shared.ticker { [weak self] (ticker, status, error) in
            self?.refreshButton.isEnabled = true
            if let ticker = ticker, status, let strongSelf = self {
                strongSelf.dateLabel.text = ticker.displayTimestamp
                strongSelf.askLabel.text = strongSelf.priceFormatter.string(for:ticker.ask) ?? ""
                strongSelf.bidLabel.text = strongSelf.priceFormatter.string(for:ticker.bid) ?? ""
                strongSelf.lastLabel.text = strongSelf.priceFormatter.string(for:ticker.last) ?? ""
            }
        }
    }
    
    @IBAction func lodHistory() {
        API.shared.tickerHistory { [weak self] (tickerHistory, status, error) in
            if let tickerHistory = tickerHistory, status, let strongSelf = self {
                let items = tickerHistory.data
                strongSelf.performSegue(withIdentifier: "toHistory", sender: items)
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let historyVC = segue.destination as? HistoryViewController,
            let items = sender as? [TickerHistoryItem] {
            historyVC.items = items
        }
        
    }
}
