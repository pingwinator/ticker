//
//  Formatters.swift
//  Ticker
//
//  Created by Vasyl Liutikov on 02.02.18.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation
extension NumberFormatter {
    @nonobjc class var priceUSD: NumberFormatter {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencyCode = "USD"
        currencyFormatter.maximumFractionDigits = 2
        return currencyFormatter
    }
}
