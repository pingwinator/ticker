//
//  VLError.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 01.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation

public enum VLError: Error, CustomStringConvertible, CustomDebugStringConvertible {
    /// can't create model
    case mapping(String)
    /// host not reachable
    case noInternet
    ///
    case badRequest
    
    public var description: String {
        switch self {
            
        case .mapping:
            return "Something wrong"
        case .noInternet:
            return "No internet connection"
            
        case .badRequest:
            return "Can't load"
        }
    }
    
    public var debugDescription: String {
        switch self {
            
        case .mapping(let objectName):
            return "object mapping error: \(objectName)"
        case .noInternet:
            return "internet.connection.unavailable"
            
        case .badRequest:
            return "cant.load"
        }
    }
}
