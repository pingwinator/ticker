//
//  SwiftyHelper.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 02.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation
import SwiftyJSON

extension JSON {
    func value(_ file: String = #file, _ line: UInt = #line) throws -> Bool {
        guard let boolValue = bool else { throw VLError.mapping(error(file: file, line: line)) }
        return boolValue
    }
    func value(_ file: String = #file, _ line: UInt = #line) throws -> Int {
        guard let intValue = int else { throw VLError.mapping(error(file: file, line: line)) }
        return intValue
    }
    func value(_ file: String = #file, _ line: UInt = #line) throws -> UInt {
        guard let intValue = uInt else { throw VLError.mapping(error(file: file, line: line)) }
        return intValue
    }
    func value(_ file: String = #file, _ line: UInt = #line) throws -> Double {
        guard let doubleValue = double else { throw VLError.mapping(error(file: file, line: line)) }
        return doubleValue
    }
    func value(_ file: String = #file, _ line: UInt = #line) throws -> String {
        guard let stringValue = string else { throw VLError.mapping(error(file: file, line: line)) }
        return stringValue
    }
    func arrayValue(_ file: String = #file, _ line: UInt = #line) throws -> [JSON] {
        guard let arrayValue = array else { throw VLError.mapping(error(file: file, line: line)) }
        return arrayValue
    }
    func dictionaryValue(_ file: String = #file, _ line: UInt = #line) throws -> [String: JSON] {
        guard let dictValue = dictionary else { throw VLError.mapping(error(file: file, line: line)) }
        return dictValue
    }
    fileprivate func error(file: String, line: UInt) -> String {
        return "File: \((file as NSString).lastPathComponent) --- Line: \(line)"
    }
}
