//
//  TokenAdapter.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 01.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Alamofire
import CryptoSwift
import Foundation

public final class TokenAdapter: RequestAdapter {

    fileprivate let token: Token

    public init(token: Token) {
        self.token = token
    }

    static let authHeader = "X-signature"
    
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        let unixtime = Int(Date().timeIntervalSince1970)
        let payload = "\(unixtime).\(token.key)"
        let hmac = try HMAC(key: token.secret, variant: .sha256)
        let auth = try hmac.authenticate(payload.bytes)
        let digest = auth.toHexString()
        let finalPayload = "\(payload).\(digest)"
        urlRequest.setValue(finalPayload, forHTTPHeaderField: TokenAdapter.authHeader)
        return urlRequest
    }
}
