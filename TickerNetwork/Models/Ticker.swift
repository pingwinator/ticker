//
//  Ticker.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 02.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

public struct Ticker: JSONDecodableModel, Codable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let ask = "ask"
        static let bid = "bid"
        static let last = "last"
        static let timestamp = "timestamp"
        static let displayTimestamp = "display_timestamp"
    }
    
    // MARK: Properties
    
    public var ask: Double
    public var bid: Double
    public var last: Double
    public var timestamp: Date
    public var displayTimestamp: String
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) throws {
        ask = try json[SerializationKeys.ask].value()
        bid = try json[SerializationKeys.bid].value()
        last = try json[SerializationKeys.last].value()
        let unixtime: Double = try json[SerializationKeys.timestamp].value()
        timestamp = Date(timeIntervalSince1970: unixtime)
        displayTimestamp = try json[SerializationKeys.displayTimestamp].value()
    }
    
}
