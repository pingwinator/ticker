//
//  Token.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 02.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation

public struct Token {
    public let key: String
    public let secret: String
    
    public init(key: String, secret: String) {
        self.key = key
        self.secret = secret
    }
}
