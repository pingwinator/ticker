//
//  TickerHistoryItem.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 02.02.18.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

public struct TickerHistoryItem: JSONDecodableModel, Codable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let average = "average"
        static let displayTimestamp = "time"
    }
    
    // MARK: Properties
    
    public var average: Double
    public var displayTimestamp: String
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public init(json: JSON) throws {
        average = try json[SerializationKeys.average].value()
        displayTimestamp = try json[SerializationKeys.displayTimestamp].value()
    }
    
}
