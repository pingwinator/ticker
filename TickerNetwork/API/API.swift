//
//  API.swift
//  TickerNetwork
//
//  Created by Vasyl Liutikov on 01.02.2018.
//  Copyright © 2018 pingwinator. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

internal typealias ServiceResponse = (JSON?, _ success: Bool, _ error: VLError?) -> Void
public typealias TickerResponse = (Ticker?, _ success: Bool, _ error: VLError?) -> Void
public typealias TickerHistoryResponse = (CollectionResponse<TickerHistoryItem>?, _ success: Bool, _ error: VLError?) -> Void

public final class API {
    public static let shared = API()
    let sessionManager: SessionManager
    let baseURLString = "https://apiv2.bitcoinaverage.com/"
    public var tokenAdapter: TokenAdapter? = nil {
        didSet {
            sessionManager.adapter = tokenAdapter
        }
    }
    
    fileprivate init() {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = defaultHeaders
        
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    fileprivate func isReachable() -> Bool {
        return VLReachabilityManager.shared.isReachable || VLReachabilityManager.shared.networkReachabilityStatus == VLNetworkReachabilityStatus.unknown
    }
    
    internal func operationWith(request:URLRequestConvertible, onCompletion: @escaping ServiceResponse) {
        guard isReachable() else {
            onCompletion(nil, false, VLError.noInternet)
            return
        }
        
        let dataRequest = sessionManager.request(request).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                onCompletion(JSON(value), true, nil)
            case .failure:
           
                onCompletion(nil, false, nil)
            }
        }
        //put breakpoint here for debug requests
        _ = dataRequest.progress
        //        debugPrint(dataRequest)
    }
    
    public func testCall() {
        let url = "https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCUSD"
        let dataRequest = sessionManager.request(url).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                print(value)
            case .failure(let er):
                
                print(er)
            }
        }
        //put breakpoint here for debug requests
        _ = dataRequest.progress
    }
    
    public func ticker(_ onCompletion: @escaping TickerResponse) {
        let urlStr = "https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCUSD"
        
        guard let url = try? urlStr.asURL() else {
            onCompletion(nil, false, VLError.badRequest)
            return
        }
        let request = URLRequest(url:url)
        operationWith(request: request) { json, success, error in
            let mapper: ModelResponseSerializer<Ticker> = ModelResponseSerializer()
            mapper.handleCompletionHandler(data: json, success, error, onCompletion)
        }
    }
    
    public func tickerHistory(_ onCompletion: @escaping TickerHistoryResponse) {
        let urlStr = "https://apiv2.bitcoinaverage.com/indices/global/history/BTCUSD?period=daily&?format=json"
        
        guard let url = try? urlStr.asURL() else {
            onCompletion(nil, false, VLError.badRequest)
            return
        }
        let request = URLRequest(url:url)
        operationWith(request: request) { json, success, error in
            let mapper: ModelResponseSerializer<CollectionResponse<TickerHistoryItem>> = ModelResponseSerializer()
            mapper.handleCompletionHandler(data: json, success, error, onCompletion)
        }
    }
}
